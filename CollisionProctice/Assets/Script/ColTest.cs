using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColTest : MonoBehaviour
{
    // Start is called before the first frame update
    public AIData data;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        CollisionAvoid.CheckMobsInRange(data);
    }
    private void OnDrawGizmos()
    {
        
        if (data != null)
        { 
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(this.transform.position, data.m_fRadius);
            Gizmos.DrawLine(this.transform.position, this.transform.position + transform.forward * data.m_fProbeLength);
            Gizmos.color = Color.yellow;
            Vector3 LeftStart = this.transform.position - transform.right * data.m_fRadius;
            Vector3 LeftEnd = LeftStart + transform.forward * data.m_fProbeLength;
            Gizmos.DrawLine(LeftStart, LeftEnd);
            Vector3 RightStart = this.transform.position + transform.right * data.m_fRadius;
            Vector3 RightEnd = RightStart + transform.forward * data.m_fProbeLength;
            Gizmos.DrawLine(RightStart, RightEnd);
            Gizmos.DrawLine(LeftEnd, RightEnd);
        }
    }
}
