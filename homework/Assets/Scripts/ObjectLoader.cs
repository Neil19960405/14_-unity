using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ObjectLoader : MonoBehaviour
{
    List<ObjectPooler> objectPool;
    //Init()
    //Load()
    //Unload()
   
    [SerializeField]
    int size = 10;
    void Start()
    {
      
        Init(size);
    }

    // Update is called once per frame
    public void Init(int size)
    {
        this.size = size;
        objectPool = new List<ObjectPooler>();
        for (int i = 0; i < size; i++)
        {
            var rt = Resources.Load("Cube");
            var rtGO = rt as GameObject;
            rtGO.SetActive(false);
            var go3 = GameObject.Instantiate(rtGO);
            objectPool.Add( new ObjectPooler { gameobject = go3,isUsing=false});
        }
    }
    public void Load()
    {
        
        var OPLength = objectPool.Where(o => o.isUsing).Count();
       if(OPLength == size)
        {
            return;
        }
        var target = objectPool.First( o =>  o.isUsing == false);
        if (target == null) return;
        target.isUsing = true;
        target.gameobject.SetActive(true);
    }
    public void Unload(GameObject removeTG)
    {
        var OPLength = objectPool.Where(o => o.isUsing==false).Count();
        if (OPLength == size)
        {
            return;
        }
        var target = objectPool.First(o => o.gameobject==removeTG);
        if (target == null) return;
        target.isUsing = false;
        target.gameobject.SetActive(false);
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Load();
        }
       
    }
}

public class ObjectPooler
{
    public GameObject gameobject;

    public bool isUsing;

}
