using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCmover : MonoBehaviour
{
    int PointNum=0;
    List<string> CheckPoints;
    Transform target;
   [SerializeField]
    float speed = 10f;
    void MoveTo(Vector3 destination)
    {
        if (Vector3.Distance(transform.position, destination) < .1f) return;
        transform.forward = (destination - transform.position).normalized;
        transform.position += transform.forward*speed*Time.deltaTime;    
    }


    private void Start()
    {
        
        CheckPoints = new List<string>();
        var CheckSet = FindObjectsOfType<CheckPoint>();
        
        while (CheckPoints.Count < CheckSet.Length)
        {
            for (int i = 0; i < CheckSet.Length; i++)
            {
                if(PointNum == CheckSet[i].GetNum)
                {
                    CheckPoints.Add(CheckSet[i].gameObject.name);
                }
            }
            PointNum++;
        }
        if (CheckSet.Length > 0) { 
        target = GameObject.Find("CheckPoint1").transform;
        }
        else
        {
            target = GameObject.Find("Sphere").transform;
        }
    }
    private void Update()
    {
        MoveTo(target.position);
    }
    private void OnTriggerEnter(Collider other)
    {
        
        void pathRun(List<string> checks,int key)
        {
            if (key > (checks.Count - 1))
            {
                return;
            }
            else
            {
                if (other.name == checks[key])
                {
                    if(key+1 > (checks.Count - 1))
                    {
                        target = GameObject.Find("Sphere").transform;
                        return;
                    }
                    target = GameObject.Find(checks[key + 1]).transform;
                }
                pathRun(CheckPoints, key + 1);
            }
        }
        pathRun(CheckPoints, 0);
        if (other.name == "Sphere")
        {
            if (CheckPoints.Count>0) { 
               target = GameObject.Find(CheckPoints[0]).transform;
            }
            else
            {
                target = GameObject.Find("Sphere").transform;
            }
        }

    } 
}
