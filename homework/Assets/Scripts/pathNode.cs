using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pathNode : MonoBehaviour
{
    public Vector3 spanwPoint;

    private void Start()
    {
        spanwPoint = new Vector3(-1.044f, 1.338f, -0.2088321f);
    }
    private void OnTriggerEnter(Collider other)
    {
       if(other.tag == "NPC")
        {
            var loader = FindObjectOfType<ObjectLoader>();
            loader.Unload(other.gameObject);
            other.transform.position = spanwPoint;
        }
    }
}
