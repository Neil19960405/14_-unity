﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AStar {
    WatPointTerrain m_Terrain;

    List<PathNode> m_OpenList;
    List<PathNode> m_CloseList;

    List<Vector3> m_PathList;

    static public AStar m_Instance;

    // Use this for initialization
    public void Init (WatPointTerrain terrain) {
        m_Terrain = terrain;
        m_OpenList = new List<PathNode>();
        m_CloseList = new List<PathNode>();
        m_PathList = new List<Vector3>();
        m_Instance = this;
    }

    public List<Vector3> GetPath()
    {
        return m_PathList;
    }

    private bool CheckCloseList(PathNode node)
    {
        foreach(PathNode n in m_CloseList)
        {
            if(n == node)
            {
                return true;
            }
        }

        return false;
    }

    private PathNode GetBestNode()
    {
        PathNode rn = null;

        float fMax = 10000.0f;

        foreach(PathNode n in m_OpenList)
        {
            if(n.m_fF < fMax)
            {
                fMax = n.m_fF;
                rn = n;
            }
        }

        m_OpenList.Remove(rn);

        return rn;
    }

    private void BuildPath(Vector3 sPos, Vector3 ePos, PathNode sNode, PathNode eNode)
    {
        m_PathList.Clear();

        m_PathList.Add(sPos);

        if(sNode == eNode)
        {
            m_PathList.Add(sNode.m_Pos);
        } else
        {
            PathNode pNode = eNode;
            while(pNode != null)
            {
                m_PathList.Insert(1, pNode.m_Pos);
                pNode = pNode.m_Parent;
            }
        }

        m_PathList.Add(ePos);
    }
	
    public bool PerformAStar(Vector3 sPos, Vector3 ePos)
    {
        int iSF = 0;
        int iEF = 0;
        if(sPos.y > 4)
        {
            iSF = 1;
        }
        if (ePos.y > 4)
        {
            iEF = 1;
        }
        PathNode sNode = m_Terrain.GetNodeFromPosition(sPos, iSF);
        PathNode eNode = m_Terrain.GetNodeFromPosition(ePos, iEF);
        if(sNode == null || eNode == null)
        {
            Debug.Log("Can not find node in AStar map");
            return false;
        } else if(sNode == eNode)
        {
            // Build Path.
            BuildPath(sPos, ePos, sNode, eNode);
            return true;
        }

        m_OpenList.Clear();
        m_CloseList.Clear();
        m_Terrain.ClearAStarInfo();
        PathNode nNode;
        PathNode cNode;
        m_OpenList.Add(sNode);
        
        while(m_OpenList.Count > 0)
        {
            Debug.Log("AAA");
            cNode = GetBestNode();
            if(cNode == null)
            {
                Debug.Log("Get Best Node error.");
                return false;
            }
            else if(cNode == eNode)
            {
                // Build Path.
                BuildPath(sPos, ePos, sNode, eNode);
                return true;
            }

            int inNei = cNode.m_Neibors.Count;
            Debug.Log(cNode.m_Pos + " : " + inNei);
            Vector3 vDist;
            for (int i = 0; i < inNei; i++)
            {
                nNode = cNode.m_Neibors[i];
                if (nNode.m_eNodeState == ePathNodeState.NODE_CLOSED)
                {
                    continue;
                }
                else if (nNode.m_eNodeState == ePathNodeState.NODE_OPENED)
                {
                    vDist = cNode.m_Pos - nNode.m_Pos;
                    float fNewG = cNode.m_fG + vDist.magnitude;
                    if (fNewG < nNode.m_fG)
                    {
                        nNode.m_fG = fNewG;
                        nNode.m_fF = nNode.m_fG + nNode.m_fH;
                        nNode.m_Parent = cNode;
                    }
                    continue;
                }
                nNode.m_eNodeState = ePathNodeState.NODE_OPENED;
                vDist = cNode.m_Pos - nNode.m_Pos;
                nNode.m_fG = cNode.m_fG + vDist.magnitude;
                vDist = eNode.m_Pos - nNode.m_Pos;
                nNode.m_fH = vDist.magnitude;
                nNode.m_fF = nNode.m_fG + nNode.m_fH;
                nNode.m_Parent = cNode;
                m_OpenList.Add(nNode);
                Debug.Log("Add open node");
            }

            cNode.m_eNodeState = ePathNodeState.NODE_CLOSED;
        }

        Debug.Log("BBB");
        return false;
    }
}
