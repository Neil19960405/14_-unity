using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
public class PathFinding : MonoBehaviour
{
    
    [System.NonSerialized]
    public PathNode[] NodeSet;
    public float speed = 10.0f;
    List<PathNode> Open = new List<PathNode>(); 
    List<PathNode> Close = new List<PathNode>();
    public Vector3 Target;
    public LayerMask raycastLayer;
    public PathNode MyNode;
    List<PathNode> FinalPath = new List<PathNode>(); 
    private void Start()
    {
        NodeSet = FindObjectsOfType<PathNode>();
        Target = this.transform.position;
    }

    private void OnDrawGizmos()
    {
        if (!Application.isPlaying) return;
        Gizmos.color = Color.blue;
        //Gizmos.DrawLine(this.transform.position, NodeSet[3].transform.position);
    }

    void MoveTo(Vector3 target)
    {
        if (Vector3.Distance(this.transform.position, target) < 1f) return;
        this.transform.forward = (target - this.transform.position).normalized;
        this.transform.position += this.transform.forward * speed * Time.deltaTime;
    }


    private void Update()//當做監聽來用
    {
        if (Input.GetMouseButton(0))
        {
            GetTarget();
            Astar();
            Debug.Log(FinalPath.Count);
            for (int i = 0; i < FinalPath.Count; i++)
            {
                if((i+1)< FinalPath.Count)
                {
                    Debug.DrawLine(FinalPath[i].Pos, FinalPath[i + 1].Pos);
                }
               
            }
            
           
        }
        //MoveTo(Target);
        //CollisionDetecter(this.transform.position, NodeSet[3].transform.position);
    }

    void GetTarget()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (!hit.collider.name.Equals("Plane"))
            {
                return;
            }
            Target = hit.point;
            Target.y = 2;
        }
    }

    bool CollisionDetecter(Vector3 origin,Vector3 target) {
        Vector3 diff = target - origin;
        Vector3 direction = diff.normalized;
        float dist = diff.magnitude;
        RaycastHit hit;
        bool rt = Physics.Raycast(origin, direction, out hit, dist, raycastLayer);
        //if (rt == true)
        //{
        //    Debug.DrawLine(origin, hit.point,Color.red);

        //}
     
        return rt;
    }

    void Astar()
    {
        Open.Clear();
        Close.Clear();
        FinalPath.Clear();
        MyNode.gcost = 0;
        MyNode.hcost = (Target - MyNode.Pos).magnitude;
        MyNode.parent = null;
        Open.Add(MyNode);
       
        while (Open.Count > 0)
        {
            int index =  GetMinFcostIndex(Open);
            if (!CollisionDetecter(Open[index].Pos,Target))
            {
                var LinkItem = Open[index]; 
                while(LinkItem != null)
                {
                    FinalPath.Add(LinkItem);
                    LinkItem = LinkItem.parent;
                }
                return;
            }
            foreach (PathNode Neibor in Open[index].Neibors)
            {
                var isInOpen = Open.Any(o => o == Neibor);
                var isInClose = Close.Any(o => o == Neibor);
                var newCost = Open[index].gcost + (Neibor.Pos - Open[index].Pos).magnitude; 
                if ((isInOpen||isInClose)&&Neibor.gcost<=newCost)
                {
                    continue;
                }
                Neibor.parent = Open[index];
                Neibor.gcost = newCost;
                Neibor.hcost = (Target - Neibor.Pos).magnitude;
                if (isInClose)
                {
                    Close.Remove(Neibor);
                }
                if (!isInOpen)
                {
                    Open.Add(Neibor);
                }
            }
            Close.Add(Open[index]);
            Open.Remove(Open[index]);
        }
    }
    //工具區
    int GetMinFcostIndex(List<PathNode> inputList)
    {
        float min = Mathf.Infinity;
        int minindex = 0;
        for (int i = 0; i < inputList.Count; i++)
        {
            if (inputList[i].Fcost < min)
            {
                minindex = i;
            }
        }
        return minindex;
    }

    void GetThePath()
    {

    }


}
