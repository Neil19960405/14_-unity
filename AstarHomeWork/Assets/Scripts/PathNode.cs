using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathNode : MonoBehaviour
{
    // Start is called before the first frame update
    
    public float gcost;//離原點的距離
    public float hcost;//到目標的距離
    public float Fcost => gcost + hcost;
    public PathNode parent;
    public Vector3 Pos;
    public List<PathNode> Neibors; 
    private void Start()
    {
        Pos = this.transform.position;
        
    }
    public override string ToString()
    {
        return Pos.ToString();
    }
}
